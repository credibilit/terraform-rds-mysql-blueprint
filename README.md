RDS MySQL Blueprint
===================

This Terraform module is a blueprint to MySQL RDS instance with HA

# Use

To create a db instance with this module you need to insert the following piece of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-rds-mysql-blueprint.git?ref=<VERSION>"
  account = "${var.account}"

  name = "test-acme-rds"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  vpc_id = "${aws_vpc.default.id}"
  tags = {
    Foo = "bar"
  }
  password = "123mudar"
  username = "administrator"
  multi_az = false
  skip_final_snapshot = true
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.


## Input parameters

The following parameters are used on this module:

- `account`: The AWS account number ID

- `name`: The name of the RDS instance and some components
- `subnet_ids`: A list of VPC subnet IDs
- `vpc_id`: The VPC ID
- `password`: Password for the master DB user
- `username`: Username for the master DB user

- `port`: Listener port
- `family`: The family of the DB
- `engine`: The database engine to use
- `engine_version`: The engine version to use
- `tags`: Map of tags
- `allocated_storage`: The allocated storage in gigabytes. Default: 100 (GB)
- `instance_class`: The instance type of the RDS instance. Default: db.m3.medium
- `storage_type`: Storage type: standard, gp2, io1. Default: gp2
- `skip_final_snapshot`: Determines whether a final DB snapshot is created before the DB instance is deleted. Default: false
- `db_name`: The DB name to create
- `backup_retention_period`: The days to retain backups for. Must be 1 or greater to be a source for a Read Replica. Default: 7
- `backup_window`: The backup window. Default: 04:00-06:00
- `iops`: The amount of provisioned IOPS. Default: 0
- `maintenance_window`: The window to perform maintenance in. Default: sun:06:00-sun:08:00
- `multi_az`: Specifies if the RDS instance is multi-AZ. Default: true
- `storage_encrypted`: Specifies whether the DB instance is encrypted. Default: false
- `apply_immediately`: Specifies whether any database modifications are applied immediately, or during the next maintenance window. Default: true
- `monitoring_role_arn`: The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs.
- `monitoring_interval`: The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance
- `kms_key_id`: The ARN for the KMS encryption key
- `custom_parameter_group_name`: Use a optional external custom parameter group


## Output parameters

This are the outputs exposed by this module.

- `db_instance`
- `parameter_group`
