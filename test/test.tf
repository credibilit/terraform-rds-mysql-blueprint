// VPC
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "rds-mysql-test"
  }
}

// Private subnets
resource "aws_subnet" "cache_a" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags {
    Name = "cache-a"
  }
}

resource "aws_subnet" "cache_b" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  tags {
    Name = "cache-b"
  }
}

// Security Group app
resource "aws_security_group" "app" {
  name = "rds-posgres-test-app"
  vpc_id = "${aws_vpc.default.id}"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

// Variables
variable "account" {}

// RDS with HA
module "test" {
  source  = "../"
  account = "${var.account}"

  name = "test-mysql-acme"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  vpc_id = "${aws_vpc.default.id}"
  security_groups = ["${aws_security_group.app.id}"]
  tags = {
    Foo = "bar"
  }
  password = "123mudar"
  username = "administrator"

  skip_final_snapshot = true
  multi_az = false
  auto_minor_version_upgrade = true
  copy_tags_to_snapshot = true
}

output "test" {
  value = "${module.test.db_instance}"
}

// External Parameter Group
resource "aws_db_parameter_group" "custom" {
  name   = "custom-pg-test-acme"
  family = "mysql5.6"
  parameter {
    name  = "time_zone"
    value = "Brazil/East"
  }
}

module "test2" {
  source  = "../"
  account = "${var.account}"

  name = "test-mysql2-acme"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  vpc_id = "${aws_vpc.default.id}"
  security_groups = ["${aws_security_group.app.id}"]
  tags = {
    Foo = "bar"
  }
  password = "123mudar"
  username = "administrator"

  skip_final_snapshot = true
  multi_az = false
  auto_minor_version_upgrade = true
  copy_tags_to_snapshot = true
  custom_parameter_group_name = "${aws_db_parameter_group.custom.id}"
}

output "test2" {
  value = "${module.test2.db_instance}"
}
