// DB Instance
module "rds" {
  source  = "git::https://bitbucket.org/credibilit/terraform-rds-blueprint.git?ref=0.0.13"
  account = "${var.account}"

  name            = "${var.name}"
  subnet_ids      = ["${var.subnet_ids}"]
  tags            = "${var.tags}"
  vpc_id          = "${var.vpc_id}"
  tags            = "${var.tags}"

  parameter_group_name  = "${var.custom_parameter_group_name == "" ? aws_db_parameter_group.default.id : var.custom_parameter_group_name}"
  port                  = "${var.port}"
  engine                = "${var.engine}"
  engine_version        = "${var.engine_version}"
  family                = "${var.family}"
  extra_security_groups = ["${var.extra_security_groups}"]

  allocated_storage = "${var.allocated_storage}"
  engine            = "${var.engine}"
  engine_version    = "${var.engine_version}"
  instance_class    = "${var.instance_class}"
  storage_type      = "${var.storage_type}"

  skip_final_snapshot = "${var.skip_final_snapshot}"
  db_name  = "${var.db_name}"
  password = "${var.password}"
  username = "${var.username}"

  backup_retention_period = "${var.backup_retention_period}"
  backup_window           = "${var.backup_window}"
  iops                    = "${var.iops}"
  maintenance_window      = "${var.maintenance_window}"
  auto_minor_version_upgrade = "${var.auto_minor_version_upgrade}"
  copy_tags_to_snapshot   = "${var.copy_tags_to_snapshot}"
  
  multi_az          = "${var.multi_az}"
  storage_encrypted = "${var.storage_encrypted}"
  apply_immediately = "${var.apply_immediately}"

  monitoring_role_arn = "${var.monitoring_role_arn}"
  monitoring_interval = "${var.monitoring_interval}"

  deletion_protection = "${var.deletion_protection}"
  enabled_cloudwatch_logs_exports = "${var.enabled_cloudwatch_logs_exports}"

  kms_key_id = "${var.kms_key_id}"
}

output "db_instance" {
  value = "${module.rds.db_instance}"
}

output "security_group" {
  value = "${module.rds.security_group}"
}
