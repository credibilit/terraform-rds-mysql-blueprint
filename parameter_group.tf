// Parameter Group
resource "aws_db_parameter_group" "default" {
  name   = "${var.name}"
  family = "${var.family}"

  parameter { name = "time_zone" value = "Brazil/East" }
  parameter { name = "log_bin_trust_function_creators" value = "1" }

  tags = "${var.tags}"
}

output "parameter_group" {
  value = {
    id  = "${aws_db_parameter_group.default.id}" 
    arn = "${aws_db_parameter_group.default.arn}"
  }
}
